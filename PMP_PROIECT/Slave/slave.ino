#include <Wire.h>

#define ROW_1 2
#define ROW_2 3
#define ROW_3 4
#define ROW_4 5
#define ROW_5 6
#define ROW_6 7
#define ROW_7 8
#define ROW_8 9
#define ROW_9 10
#define ROW_10 11

 const unsigned  short int rows[]= {ROW_1, ROW_2, ROW_3, ROW_4, ROW_5, ROW_6, ROW_7, ROW_8, ROW_9, ROW_10};


void setup()
{
  // initializam liniile ca si porturi de output
  for(int i=0; i<10; i++) {
    pinMode(rows[i],OUTPUT);
  }
  Serial.begin(9600);
  //asteptam mesaj de la master
  Wire.begin(4);
  Wire.onReceive(receiveEvent); 
}

void loop()
{

}
int value=1;//value este parametrul necesar pentru activarea/dezactivarea liniei
//functie de primire de mesaj de la master
void receiveEvent(int count)
{
  byte i = Wire.read();
  
  Serial.println(i);

  switch (i) {//scriem pe linia  necesara valoarea primita
  case 0:
    digitalWrite(rows[i], value); 
    break;
  case 1:
    digitalWrite(rows[i], value); 
    break;
  case 2:
    digitalWrite(rows[i], value); 
    break;
  case 3:
    digitalWrite(rows[i], value); 
    break;
  case 4:
    digitalWrite(rows[i], value); 
    break;
  case 5:
    digitalWrite(rows[i], value); 
    break;
  case 6:
    digitalWrite(rows[i], value); 
    break;
  case 7:
    digitalWrite(rows[i], value); 
    break;
  case 8:
    digitalWrite(rows[i], value); 
    break;
  case 9:
    digitalWrite(rows[i], value); 
    break;
}
 if(value==1) //negare valoarea value
 {
   value=0;
 }
  else
  {
    value=1;}
  
}
